const db = require('quick.db');
const config = require('../config.json');

module.exports.run = async (bot, message, args) => {

  let balance = db.fetch(`balance_${message.author.id}`);
  let curlvl = db.fetch(`${message.author.id}_level`);
 
  var rolelads = message.guild.roles.cache.find((role) => role.id === config.lvlrole1);
  var roleunits = message.guild.roles.cache.find((role) => role.id === config.lvlrole2);
  var roleg = message.guild.roles.cache.find((role) => role.id === config.lvlrole3);
  var roleimg = message.guild.roles.cache.find(role => role.id === config.imgperms);


  if (message.guild.id === config.guildID) {
     if (curlvl >= 9 && curlvl < 19) {
            message.member.roles.add(rolelads);
            message.channel.send("roles restored (Lads)")
     } else if (curlvl >= 19 && curlvl < 49) {
            message.member.roles.add(rolelads);
            message.member.roles.add(roleunits);
            message.channel.send("roles restored (Lads, Middle Class Workers)")
     } else if (curlvl >= 49 && curlvl < 99) {
            message.member.roles.add(rolelads);
            message.member.roles.add(roleunits);
            message.member.roles.add(roleg);
            message.channel.send("roles restored (Lads. Middle Class Workers, Upper Class Workers)")
     } else if (curlvl >= 99 && curlvl < 499) {
            message.member.roles.add(rolelads);
            message.member.roles.add(roleunits);
            message.member.roles.add(roleg);
            message.member.roles.add(roleimg);
            message.channel.send("roles restored (Lads. Middle Class Workers, Upper Class Workers, Image Perms)")
     } else {
        message.channel.send("you've stacked no paper so far...")
     }
  } else return message.channel.send("this command isn't available in this server.");

}

module.exports.help = {
  name: "restore",
  type: "user"
}

