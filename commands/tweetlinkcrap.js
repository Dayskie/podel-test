const twturl = require("twitter-url-direct");

module.exports.run = async(bot, message, args) => {
    let channel = message.channel;
    if (args[1] === "-s" || args[1] === "--silent") {
        channel = message.author;
        message.delete();
    }
    if (args[0].includes("twitter.com/")) {
        let query = await twturl(args[0]);
	let link = query.download[0].url.replace("https//", "https://")
            link = link.replace("https://ssstwitter.com", "")

        channel.send(`download link:\n ${link}`);
    } else channel.send("not a twitter.com link"); 
};

module.exports.help = {
    name: "tweetlinkcrap",
    aliases: ["tlinkc", "tlc", "tldc", "downloadtweetcrap"],
    type: "user"
};
