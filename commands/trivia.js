const superagent = require("superagent");
const db = require("quick.db");

module.exports.run = async (bot, message, args) => {
    let diff = ["medium", 250];

    if (args[0] == "medium") diff = ["medium", 250]
    else if (args[0] == "hard") diff = ["hard", 500]

    let { body } = await superagent.get(`https://opentdb.com/api.php?amount=10&category=24&difficulty=${diff[0]}&type=multiple`);
    
    let n = Math.floor(Math.random() * 10);

    let answers = body.results[n].incorrect_answers;
    answers.push(body.results[n].correct_answer);

    answers = answers.sort(() => (Math.random() > .5) ? 1 : -1);

    let filter = m => m.author.id === message.author.id;
    message.channel.send("Question: `" + body.results[n].question + "`\n**pick an answer (1-4)**\n" + answers.join('\n') + "\nyou have 18 seconds").then(() => {
        message.channel.awaitMessages(filter, {
            max: 1,
            time: 1000 * 18,
            errors: ['time']
        })
            .then(message => {
                message = message.first()
                let input = parseInt(message.content);
                if (answers[input - 1] === body.results[n].correct_answer) {
			message.channel.send(`you win, here's ${diff[1]} quid`);
			return db.add(`balance_${message.author.id}`, diff[1]);
 		} else return message.channel.send("you lose");
            })
            .catch(collected => {
                message.channel.send(`you ran out of crap time, you lost ${diff[1]} quid`);
            });
    });
};

module.exports.help = {
    name: "trivia",
    type: "user"
}
