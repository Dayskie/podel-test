const superagent = require("superagent");

module.exports.run = async (bot, message, args) => {

  let players = "-";
  let motd = "";
  let online;

  let { body } = await superagent.get("https://eu.mc-api.net/v3/server/ping/92.42.46.150");

  if (body.status == true) online = "🟢";
  else online = "🔴";

  if (body.players.online > 0) {
    players = [];
    for (var i = 0; i < body.players.sample.length; i++) {
      players += "\n- " + body.players.sample[i].name;
    }
  }

  for (var i = 0; i < body.description.extra.length; i++) {
    motd += body.description.extra[i].text;
  }

  message.channel.send(`**92.42.46.150 | 1.19.2 Vanilla** ${online}\nMOTD: \`${motd}\`\nPlayers: ${body.players.online}/${body.players.max} \`\`\`${players}\`\`\`\nServer Icon: https://eu.mc-api.net/v3/server/favicon/92.42.46.150`);

}

module.exports.help = {
  name: "mc",
  type: "user"
}
