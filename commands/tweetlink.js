const twturl = require("twitter-url-direct");

module.exports.run = async(bot, message, args) => {
    let channel = message.channel;
    if (args[1] === "-s" || args[1] === "--silent") {
        channel = message.author;
        message.delete();
    }
    if (args[0].includes("twitter.com/")) {
        let query = await twturl(args[0]);
        for (var i = 0; i < query.dimensionsAvailable - 1; i++) {
let link = query.download[i].url.replace("https//", "https://")
    link = link.replace("https://ssstwitter.com", "")
            channel.send(`\`${query.download[i].dimension}\`\n ${link}`);
        }
    } else channel.send("not a twitter.com link"); 
};

module.exports.help = {
    name: "tweetlink",
    aliases: ["tlink", "tl", "tld", "downloadtweet"],
    type: "user"
};
