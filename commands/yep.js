const Canvas = require("canvas");

module.exports.run = async (bot, message, args, member) => {
let user = message.author.avatarURL({ format: 'png', dynamic: true, size: 1024 });
if (message.mentions.users.first() !== undefined) user = message.mentions.users.first().avatarURL({ format: 'png', dynamic: true, size: 1024 });
else if (args[0]) user = args.join('');
  const canvas = Canvas.createCanvas(256, 256);
  const ctx = canvas.getContext("2d");

  const foreground = await Canvas.loadImage(
    "https://cdn.discordapp.com/attachments/453453641249062912/645560181576433665/yep.png"
  );

  ctx.strokeRect(0, 0, canvas.width, canvas.height);

  const avatar = await Canvas.loadImage(user);
  ctx.drawImage(avatar, 0, 0, canvas.width, canvas.height);
  ctx.drawImage(foreground, 50, 225, 150, 35);

  message.channel.send("yep", { files: [canvas.toBuffer()] });
};

module.exports.help = {
  name: "yep",
  type: "user"
};
