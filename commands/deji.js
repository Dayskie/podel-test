const Canvas = require("canvas");

module.exports.run = async (bot, message, args, member) => {
let user = message.author.avatarURL({ format: 'png', dynamic: true, size: 1024 });
if (message.mentions.users.first() !== undefined) user = message.mentions.users.first().avatarURL({ format: 'png', dynamic: true, size: 1024 });
else if (args[0]) user = args.join('');
  const canvas = Canvas.createCanvas(1280, 720);
  const ctx = canvas.getContext("2d");

  const background = await Canvas.loadImage(
     "https://cdn.discordapp.com/attachments/439127582194008065/912384861032054844/Ebene_5.png"
  );
  const avatar = await Canvas.loadImage(user);
  await ctx.drawImage(avatar, 1000, 0, 250, 720);
  await ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

  message.channel.send("", { files: [canvas.toBuffer()] });
};

module.exports.help = {
  name: "deji",
  type: "user"
}

