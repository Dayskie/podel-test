const Canvas = require("canvas");

module.exports.run = async (bot, message, args) => {

  let imageUrl = args[0];

  if (!args[0] && message.attachments.size === 0) {
    message.channel.messages.fetch().then(async (messages) => {
    
      const lastMessage = messages.sort((a, b) => b.createdTimestamp - a.createdTimestamp).filter((m) => m.attachments.size > 0).first();
       imageUrl = lastMessage.attachments.first().proxyURL;
   
       const canvas = Canvas.createCanvas(575, 535);
       const ctx = canvas.getContext("2d");

       const background = await Canvas.loadImage(
          "https://cdn.discordapp.com/attachments/718124622238711869/905216831147638836/mcrib2.png"
       );
       const img = await Canvas.loadImage(imageUrl);
       await ctx.drawImage(img, 68, 205, 505, 285);
       await ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

    return message.channel.send("they wouldn't get it", { files: [canvas.toBuffer()] });

    });
  } else if (!args[0]) { 
    message.attachments.forEach(attachment => {
       imageUrl = attachment.proxyURL;
    });
    
    const canvas = Canvas.createCanvas(575, 535);
    const ctx = canvas.getContext("2d");

    const background = await Canvas.loadImage(
       "https://cdn.discordapp.com/attachments/718124622238711869/905216831147638836/mcrib2.png"
    );
    const img = await Canvas.loadImage(imageUrl);
    await ctx.drawImage(img, 68, 205, 505, 285);
    await ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

    return message.channel.send("they wouldn't get it", { files: [canvas.toBuffer()] });

  }

  const canvas = Canvas.createCanvas(575, 535);
  const ctx = canvas.getContext("2d");

  const background = await Canvas.loadImage(
     "https://cdn.discordapp.com/attachments/718124622238711869/905216831147638836/mcrib2.png"
  );
  const img = await Canvas.loadImage(imageUrl);
  await ctx.drawImage(img, 68, 205, 505, 285);
  await ctx.drawImage(background, 0, 0, canvas.width, canvas.height);


  message.channel.send("they wouldn't get it", { files: [canvas.toBuffer()] });
};

module.exports.help = {
  name: "mcrib",
  type: "user"
}
