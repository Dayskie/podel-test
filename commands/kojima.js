const Canvas = require("canvas");

module.exports.run = async (bot, message, args, member) => {
let user = message.author.avatarURL({ format: 'png', dynamic: true, size: 1024 });
if (message.mentions.users.first() !== undefined) user = message.mentions.users.first().avatarURL({ format: 'png', dynamic: true, size: 1024 });
else if (args[0]) user = args.join('');
  const canvas = Canvas.createCanvas(1570, 2048);
  const ctx = canvas.getContext("2d");

  const background = await Canvas.loadImage(
    "https://img.pngio.com/shitpostbot-5000-hideo-kojima-png-1570_2048.png"
  );

  let kojimoji = bot.emojis.cache.find(emoji => emoji.name === `kojima`);

  ctx.strokeRect(0, 0, canvas.width, canvas.height);

  const avatar = await Canvas.loadImage(user);
  ctx.drawImage(avatar, 0, 0, canvas.width, canvas.height);
  ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

  message.channel.send(kojimoji, { files: [canvas.toBuffer()] });
};

module.exports.help = {
  name: "kojima",
  type: "user"
};
