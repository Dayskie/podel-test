const db = require("quick.db"),
    ms = require("parse-ms");
const json = require("../items.json");
const Discord = require('discord.js');
const config = require('../config.json'),
  colour = config.colour;

module.exports.run = async (bot, message, args) => {

        const mb = db.fetch(`marlboro_${message.author.id}`);

        if (mb <= 0 || mb === null) return message.channel.send('you have no cigs.');

        let list = [
            "https://cdn.discordapp.com/attachments/781615385350766662/945807703907922013/catsmikeevil.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902196609826295838/images.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902196226383036466/a3e9e41ab17f11e19dc71231380fe523_7.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902196188391026708/20814abe5a74c2264dc9ede27e50664a.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902196162809958410/2482106261_a0b9d44103_b.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902195991585910814/jEF3pE9e_400x400.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902195961546285056/6wnfxdq35ol11.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902195917057302528/hqdefault.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902195834618265630/original.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902195805455274064/images.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902195780796948570/images.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902195754972626984/08c.png",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291309602172928/images.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291226655608862/unnamed_1.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291223358881843/sa6tg6kpdhv41.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291224235503626/vugbiov4pm611.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291223061073960/unnamed.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291219919536128/maxresdefault.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291217902075974/lz7nvdztd9v21.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291214366310400/fv5z4alodla21.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291166735773778/f128e7e9ed7bd8449266d684936e43e4.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291165586550844/EJZLyqsXUAIeznf.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291164303081553/Capture.PNG",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291161580990514/4563949501_770ec9bf9c_b.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291161731964948/baf5737b77e8399b5194a7a3a345de44.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291158418456616/241756088_275129744447422_5504728915254623484_n.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291156900118558/128677915_105618188065246_7275878364326529000_n.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291155255959562/34k5hlkxddh51.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291152961671168/7e86a70cf6e3b9c48115bcffe444a866.jpg",
            "https://cdn.discordapp.com/attachments/796119027683557376/902291150847742093/6zme83rwo5uy.jpg",
	    "https://cdn.discordapp.com/attachments/317010910239522816/1000312959253368904/catsmikereal.jpg"
          ];

        let result = Math.floor(Math.random() * list.length);

        const embed = new Discord.MessageEmbed()
        .setTitle(message.author.username + ' just smiked one')
        .setImage(list[result])
        .setColor(colour)
        .setFooter('Podel, wasted smikes since 1992', bot.user.avatarURL());

        message.channel.send(embed);
        await db.subtract(`marlboro_${message.author.id}`, 1);

};

module.exports.help = {
    name: "smike",
    aliases: ["smoke"],
    type: "user"
};
