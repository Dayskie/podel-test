const { MessageAttachment } = require("discord.js");

module.exports.run = async (bot, message, args) => {

  const file = new MessageAttachment("../gilgamesh.mp3"); 
  message.channel.send("<:sisyphus:944970346346479686>", file);

}

module.exports.help = {
  name: "sisyphus",
  aliases: ["gilgamesh"],
  type: "user"
}
