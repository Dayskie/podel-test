const superagent = require("superagent");

module.exports.run = async (bot, message, args) => {

  let { body } = await superagent.get("https://tenor.googleapis.com/v2/search?q=lietuva&key=AIzaSyACVn_X4z95aIhrH1cqkNbPNT6zqNaIUaI&random=true");

  message.channel.send(body.results[0].media_formats.gif.url);
  
}

module.exports.help = {
  name: "lithuania",
  aliases: ["lietuva", "liet", "lietu"],
  type: "user"
}
