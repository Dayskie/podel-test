# cristpz/Podel
The official Podel Bot Repository by cristpz
## How to set-up and run Podel Bot
1. Get [Node.js](https://nodejs.org/en/download/)
2. Get [Git](https://git-scm.com/downloads)
3. Clone [Podel](https://gitlab.com/cristpz1/podel.git).
4. Run `npm i`, `cd music`, run `npm i` again, don't close the terminal.
5. Change [secret.json](https://gitlab.com/cristpz1/podel/-/blob/main/podel.js#L2) and [music/secret.json](https://gitlab.com/cristpz1/podel/-/blob/main/music/index.js#L2) to your own files including the following:
```json
{
 "token": "discord_bot_token",
 "pexapi": "https://www.pexels.com/api/"
}
```
6. Run `node podel.js` and `node music/index.js` for the music commands and the video hosting website.
7. If you don't want the video hosting website or the music interface, [click here](https://gitlab.com/cristpz1/podel/-/wikis/Getting-rid-of-Podel-Music's-Web-UI-and-Video-Hosting).
## To-do's
- [x] replace mee6's XP and autorole system.
- [x] add a shop and finally use json instead of coding the items inside the shop.js file (wtf is wrong with me).
- [x] fix all exploits.
- [x] replace crapboat lol.
- [x] provide food and shelter to my family of 2 wives and 12 children in baghdad.
## Authors
-  [cristpz](https://github.com/cristpz/)
-  [brexite](https://github.com/brexite/)
## Support & Contact
Just DM **crist#0971** on Discord or send an **e-mail** to [contact@cristpz.eu](mailto://contact@cristpz.eu).
## FAQ (if anyone asks, I am not 350,000 USD in debt):
Q: can I kill all your time by asking questions about every part of the bot's code
A: yes
Q: are you employed?
A: no :)
Q: what's the actual point of podel bot?
A: replacing stupid fucking bots like mee6. we're 100% open to pull requests and feedback 
Q: can I have a go in your 1997 Citroën Saxo VTS?
A: yes :D