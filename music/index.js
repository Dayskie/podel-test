// Settings above all because I'm lazy
const secret = require("../../secret.json");
const settings = {
    prefix: 'p!',
    token: secret.token
};

const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');

const commands = [
    {
        name: 'skip',
        description: 'skips the crap song that is playing right now',
    },
    {
        name: 'khaled',
        description: 'dj',
    },
    {
        name: 'k',
        description: 'dj',
    },
    {
        name: 'die',
        description: 'could be me',
    },
    {
        name: 'shuffle',
        description: 'will make the queue look like the greek economy',
    },
    {
        name: 'clear',
        description: 'removes everything from the queue',
    },
    {
        name: 'np',
        description: 'displays what song is playing like right now',
    },
    {
        name: 'q',
        description: 'shows the queue',
    },
    {
        name: 'pause',
        description: 'pauses the current song',
    },
    {
        name: 'resume',
        description: 'resumes the current song',
    }
];

const rest = new REST({ version: '9' }).setToken(settings.token);

(async () => {
    try {
        console.log('Started refreshing application (/) commands.');

        await rest.put(
            Routes.applicationGuildCommands(secret.client_id),
            { body: commands },
        );

        console.log('Successfully reloaded application (/) commands.');
    } catch (error) {
        console.error(error);
    }
})();


const {
    Client,
    Intents
} = require('discord.js');

const yts = require("yt-search");

const db = require("quick.db");

const client = new Client({
    intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_VOICE_STATES, Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS]
});

const { Player } = require("discord-music-player");
const player = new Player(client, {
    leaveOnEmpty: true,
    quality: 'high'
    /*ytdlRequestOptions: {
      headers: {
        cookie: "CONSENT=PENDING+565; SOCS=CAISEwgDEgk0NzcxNTY5NTUaAmVuIAEaBgiAwNOZBg; VISITOR_INFO1_LIVE=BDtTVmkn_Pc; LOGIN_INFO=AFmmF2swRQIhAL5OAbr9ghKQQCzPk3ZOe74te4Ev7TdFxRw0et_mjJt3AiBpO4BFRlVj7ZCrrQoubBAk_DHoemfPerXiQa-v0GKzrA:QUQ3MjNmeFRSR1ZlX0hTRjZaYlJRVld2eVg0R3l2S3BhcE1HMTliMUxuMXQ0SUVEeHZIQWtDMDBnVUdTRU9nbHNLZHoxYmRoZG9fclBDZmlhOVViNHpqQXpTRHdzRS1IdUgzaE1qczN3STAzd2plTEhnMUpKMlhKR0ZQQ1kweUV6cGd6cTYxaHQtSC1Zem1aZFFjMU5pQjF2VWgzeXh1UV93; HSID=AkmECXEXz2DfHb1zA; SSID=AoX4JkxhA8WDC1Sbi; APISID=2gcpSOTc3gXP5jvd/AdTwe8gj5cf2dSy12; SAPISID=Z3L5yM6VQOAJ86T_/AElSykVV2FfW8I00M; __Secure-1PAPISID=Z3L5yM6VQOAJ86T_/AElSykVV2FfW8I00M; __Secure-3PAPISID=Z3L5yM6VQOAJ86T_/AElSykVV2FfW8I00M; PREF=tz=Europe.Athens&autoplay=true&f5=20000; SID=Qwj4Z-sn46FKEQbgRlt5h-LkjLmQ7c4He7h27cbvdmRU0HZyNiAGNGdXn3eB-rdBuZxxng.; __Secure-1PSID=Qwj4Z-sn46FKEQbgRlt5h-LkjLmQ7c4He7h27cbvdmRU0HZyTGo0fPyfp09NyuxJEuowcw.; __Secure-3PSID=Qwj4Z-sn46FKEQbgRlt5h-LkjLmQ7c4He7h27cbvdmRU0HZynltnNmFSfVlHy_DWzh__2A.; YSC=xOqaNmlohCA; SIDCC=AIKkIs3mioqRkhDv_alsJrHYUcGtPHO-CVBbOxKf47iGBWhfbsUC-xB16RYt13rVlms8lO0Emw; __Secure-1PSIDCC=AIKkIs3jtsu31DHT3XUHZfuXyCWXZNAcGyiCOe_83goKfGxK9EkWTOWblOCPxOvG5dWGGbBtRXI; __Secure-3PSIDCC=AIKkIs0bc3AJkLxaNfjVik9xpU6vUh7spf1gLJlacegFF3idpet5fPp2kzBaIi_WkPiJpWmPqQ",
        'x-youtube-identity-token': "QUFFLUhqbkY5LUN6MFl5NURVSmpQbHhZRU9oVmxhc0xrQXw=",
      }
    }*/
});

client.player = player;

client.player
    .on('songAdd', (queue, song) =>
        queue.data.channel.send(`${song} was added to the queue.\nweb interface: https://johnjohnspub.com/pbmui.html?guild_id=${queue.data.guild_id}`))
    .on('playlistAdd', (queue, playlist) =>
        queue.data.channel.send(`playlist ${playlist} with ${playlist.songs.length} was added to the queue.\nweb interface: https://johnjohnspub.com/pbmui.html?guild_id=${queue.data.guild_id}`))

client.on("ready", () => {
    console.log("podel music ready");
});

const { createAudioPlayer, createAudioResource, joinVoiceChannel } = require('@discordjs/voice');
const { RepeatMode } = require('discord-music-player');

const fs = require('fs');

const express = require('express');
const app = express();

let options = {
    key: fs.readFileSync('/etc/letsencrypt/live/johnjohnspub.com/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/johnjohnspub.com/cert.pem'),
    ca: fs.readFileSync('/home/crist/easy-rsa/pki/ca.crt')
};

let https = require('https'),
    server = https.createServer(options, app);

const cors = require('cors');
app.use(cors());

const se = require("serve-index");

app.use('/', express.static('/var/www/html/'));
app.use('/shop', se('/var/www/html/shop'));
app.use('/videos', se('/var/www/html/videos'));

app.get('/channel', function (req, res) {
    res.redirect("https://www.youtube.com/watch?v=-QKGj8g9gy4&feature=youtu.be");
});

app.get('/player', function (req, res) {
    res.sendFile("/var/www/html/player.html");
});

app.get('/np', function (req, res) {
    if (!isNaN(req.query.guild_id)) {

        let guildQueue = client.player.getQueue(req.query.guild_id);

        if (!guildQueue) return res.status(200).json({ np: "nothing is playing on this guild at the moment", queue: "" });

        const ProgressBar = guildQueue.createProgressBar();

        let list = guildQueue.songs.map((song, i) => {
            if (song && i !== 0) return `<h3>${`${i + 1}`} - ${song.name} | ${song.author} &nbsp;<span class="button" onClick="remove(${i})">[x]</span>`
        }).join(`</h3>`);

        let songRes = `${guildQueue.nowPlaying} | ${guildQueue.volume}%<br>${ProgressBar.prettier}`;
        let queueRes = `${list}`

        res.status(200).json({
            np: songRes,
            queue: queueRes
        });
    } else res.status(200).json({
        np: "no guild_id provided. e.g: https://johnjohnspub.com/pbmui.html?guild_id=000000000000000000",
        queue: ""
    });
});

app.get('/skip', async function (req, res) {
    let guild = client.guilds.cache.get(req.query.guild_id);
    let userid = await db.get(`${req.query.key}_${req.query.guild_id}`);
    if (!isNaN(req.query.guild_id) && guild.me.voice && userid !== null) {
        let guildQueue = client.player.getQueue(req.query.guild_id);
        if (guildQueue) return guildQueue.skip(), res.end();
        res.end();
    } else res.end();
});

app.get('/pause', async function (req, res) {
    let guild = client.guilds.cache.get(req.query.guild_id);
    let userid = await db.get(`${req.query.key}_${req.query.guild_id}`);
    if (!isNaN(req.query.guild_id) && guild.me.voice && userid !== null) {
        let guildQueue = client.player.getQueue(req.query.guild_id);
        if (guildQueue) return guildQueue.setPaused(true), res.end();
        res.end();
    } else res.end();
});

app.get('/resume', async function (req, res) {
    let guild = client.guilds.cache.get(req.query.guild_id);
    let userid = await db.get(`${req.query.key}_${req.query.guild_id}`);
    if (!isNaN(req.query.guild_id) && guild.me.voice && userid !== null) {
        let guildQueue = client.player.getQueue(req.query.guild_id);
        if (guildQueue) return guildQueue.setPaused(false), res.end();
        res.end();
    } else res.end();
});

app.get('/rm', async function (req, res) {
    let guild = client.guilds.cache.get(req.query.guild_id);
    let userid = await db.get(`${req.query.key}_${req.query.guild_id}`);
    if (!isNaN(req.query.guild_id) && guild.me.voice && userid !== null) {
        let guildQueue = client.player.getQueue(req.query.guild_id);
        if (guildQueue) return guildQueue.remove(req.query.song_index), res.end();
        res.send(req.query.song_index);
    } else res.end();
});

/* 
VIDEO HOSTING START
*/


const superagent = require("superagent");
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const virDir = path.join(__dirname, 'files');
const extractFrames = require("ffmpeg-extract-frames");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.json());

app.use("/files", express.static(path.join(__dirname, "/files/")));
app.use("/thumbnail", express.static(path.join(__dirname, "/thumbnails/")));

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './files');
    },
    filename: async function (req, file, callback) {
        var fileName = file.originalname.toLowerCase().split(' ').join('-');
        if (fileName.endsWith(".mp4") || fileName.endsWith(".webm") || fileName.endsWith(".mov")) {
            fs.readdir(virDir, function (err, files) {
                if (err) {
                    return console.log('Unable to scan directory: ' + err);
                }
                files.forEach(function (filnam) {
                    if (filnam === fileName) fileName = fileName.replace(`${fileName.split(".").pop()}`, `_.${fileName.split(".").pop()}`);
                });
            });

            extractFrames({
                input: `./files/${fileName}`,
                output: `./thumbnails/${fileName}.jpg`,
                offsets: [1000]
            });
            db.add("latestNum", 1);
            callback(null, fileName);
            db.set(`${db.fetch("latestNum")}_fname`, fileName);
        } else if (fileName.endsWith(".jpg") || fileName.endsWith(".png") || fileName.endsWith(".gif") || fileName.endsWith(".jpeg") || fileName.endsWith(".jfif")) {
            fs.readdir(virDir, function (err, files) {
                if (err) {
                    return console.log('Unable to scan directory: ' + err);
                }
                files.forEach(function (filnam) {
                    if (filnam === fileName) fileName = fileName.replace(`${fileName.split(".").pop()}`, `_.${fileName.split(".").pop()}`);
                });
            });

            db.add("latestP", 1);
            callback(null, fileName);
            db.set(`${db.fetch("latestP")}_pname`, fileName);
        } else if (fileName.endsWith(".mp3") || fileName.endsWith(".wav") || fileName.endsWith(".flac") || fileName.endsWith(".ogg") || fileName.endsWith(".m4a")) {
            fs.readdir(virDir, function (err, files) {
                if (err) {
                    return console.log('Unable to scan directory: ' + err);
                }
                files.forEach(function (filnam) {
                    if (filnam === fileName) fileName = fileName.replace(`${fileName.split(".").pop()}`, `_.${fileName.split(".").pop()}`);
                });
            });

            db.add("latestA", 1);
            callback(null, fileName);
            db.set(`${db.fetch("latestA")}_aname`, fileName);
        }
    }
});

var upload = multer({
    storage: storage,
    limits: { fileSize: 52428800 }
}).single('video');

app.get('/video/auto', async function (req, res) {
    let vids = db.all().filter((data) => data.ID.endsWith("video")).sort((a, b) => b.data - a.data);
    vids.length = 200;

    var i = 0;

    let list = [];

    for (i in vids) {
        let vidnam = db.fetch(`${vids[i].ID.split('_')[0]}_fname`);
        list.push(`${vidnam}`);
    }

    let result = Math.floor(Math.random() * list.length);

    let link = list[result];

    res.send(`
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta property="og:video" content="https://johnjohnspub.com/files/${link}" />
    <meta property="og:video:secure_url" content="https://johnjohnspub.com/files/${link}" />
    <meta property="og:video:type" content="application/x-shockwave-flash" />
    <meta property="og:video:width" content="600" />
    <meta property="og:video:height" content="400" />
    <title>johnjohnspub.com | ${link}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        body {
            width: 100%;
            height: 100%;
            max-height: 1500px;
            margin: 40px auto;
            max-width: 1500px;
            background-color: #000;
        }
    </style>
<script>
function copy() {

  var copyText = document.getElementById("linkCopy");

  copyText.select();
  copyText.setSelectionRange(0, 99999); 

  document.execCommand("copy");
  
}
document.getElementById('playerHater').addEventListener('ended', refresh, false);
function refresh(e) {
    window.location.reload();
}
</script>
</head>

<body>
    <video id="playerHater" style="width:100%;height:100%;max-width:1000px;max-height:720px;" autoplay controls>
        <source src="https://johnjohnspub.com/files/${link}" type="video/mp4">
    </video>
    <h1 style="color:#fff;padding-left:auto;">spongebob carpetbombing moments</h1>
    <a style="font-size:28px;" href="/video/main">back to main</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="https://johnjohnspub.com/files/${link}" id="linkCopy"> <button onclick="copy()">To Clipboard</button>&nbsp;<button onclick="window.location.reload();">Random Video</button>
    <p></p>
</body>
</html>
    `);
});

app.get('/video/autoim', async function (req, res) {
    let pics = db.all().filter((data) => data.ID.endsWith("photo")).sort((a, b) => b.data - a.data);
    pics.length = 200;

    var i = 0;

    let list = [];

    for (i in pics) {
        let img = db.fetch(`${pics[i].ID.split('_')[0]}_pname`);
        list.push(`${img}`);
    }

    let result = Math.floor(Math.random() * list.length);

    let link = list[result];

    res.send(`
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta property="og:image" content="https://johnjohnspub.com/files/${link}" />
    <title>johnjohnspub.com | ${link}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        body {
            width: 100%;
            height: 100%;
            max-height: 1500px;
            margin: 40px auto;
            max-width: 1500px;
            background-color: #000;
        }
    </style>
<script>
function copy() {

  var copyText = document.getElementById("linkCopy");

  copyText.select();
  copyText.setSelectionRange(0, 99999); 

  document.execCommand("copy");
  
}
</script>
</head>

<body>
   <center> 
   <br><br><br>
   <img src="https://johnjohnspub.com/files/${link}" style="width:100%;height:100%;max-width:800px;max-height:600px;">
   <h1 style="color:#fff;padding-left:auto;"> I added a refresh button (I'm jobless)</h1>
   <a style="font-size:28px;" href="/video/images">back to images</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="https://johnjohnspub.com/files/${link}" id="linkCopy"> <button onclick="copy()">To Clipboard</button>&nbsp;<button onclick="window.location.reload();">Random Image</button>
   <p></p>
   </center>
</body>
</html>
    `);
});

app.get('/video/', function (req, res) {
    res.sendFile(__dirname + "/index.html");
});

app.get('/video/dioau', function (req, res) {
    res.sendFile(__dirname + "/dioau.html");
});

app.post('/comment', function (req, res) {
    // if no post data do nothing
    if (req.body.length < 1) return res.redirect(`/watch?v=${req.body.videoname}`);

    // list of stuff that could mess up the page
    let forbidden = ["<script>", "<head>", "<meta>", "color:#000", "color:black", "<iframe>", "autoplay"];

    // crap check 
    for (var i = 0; i < forbidden.length; i++) {
        if (req.body.username.includes(forbidden[i]) || req.body.comment.includes(forbidden[i])) return res.redirect(`/watch?v=${req.body.videoname}`);
    }

    let index = 0;

    while (db.fetch(`${req.body.videoname}_comment_${index}_username`) != undefined) index++;

    db.set(`${req.body.videoname}_comment_${index}_username`, req.body.username);
    db.set(`${req.body.videoname}_comment_${index}_content`, req.body.comment);

    res.redirect(`/watch?v=${req.body.videoname}`);
});

app.post('/video/upload', function (req, res) {
    upload(req, res, function (err) {
        if (req.body.title.length > 256 || req.body.username.length > 256) return res.redirect(`/`);

        if (err) {
            db.delete(`${db.fetch("latestNum")}_fname`);
            db.subtract("latestNum", 1);
            return res.end("Error uploading file.");
        }

        if (req.body.username.length > 0) db.set(`${db.fetch("latestNum")}_username`, req.body.username);
        db.set(`${db.fetch("latestNum")}_video`, req.body.title);
        res.end("File uploaded successfully!");
        //var exec = require("child_process").exec;
        //exec("node vidmsg.js", function() {});
    });
});

app.post('/video/imupload', function (req, res) {
    upload(req, res, function (err) {

        if (err) {
            db.delete(`${db.fetch("latestP")}_pname`);
            db.subtract("latestP", 1);
            return res.end("Error uploading file.");
        }

        if (req.body.username.length > 0) db.set(`${db.fetch("latestP")}_username`, req.body.username);
        db.set(`${db.fetch("latestP")}_photo`, req.body.title);
        res.end("File uploaded successfully!");
        //var exec = require("child_process").exec;
        //exec("node picmsg.js", function() {});
    });
});

app.post('/video/adupload', function (req, res) {
    upload(req, res, function (err) {

        if (err) {
            db.delete(`${db.fetch("latestA")}_aname`);
            db.subtract("latestA", 1);
            return res.end("Error uploading file.");
        }

        if (req.body.username.length > 0) db.set(`${db.fetch("latestA")}_username`, req.body.username);
        db.set(`${db.fetch("latestA")}_audio`, req.body.title);
        res.end("File uploaded successfully!");
        //var exec = require("child_process").exec;
        //exec("node audiomsg.js", function() {});
    });
});

app.use("/video/main", async function (req, res) {

    let vids = db.all().filter((data) => data.ID.endsWith("video")).sort((a, b) => b.data - a.data);
    let list = [];

    for (var i = vids.length - 1; i >= 0; i--) {
        let username = db.fetch(`${vids[i].ID.split('_')[0]}_username`);
        let vidnam = db.fetch(`${vids[i].ID.split('_')[0]}_fname`);
        if (username === null) username = "anonymous";
        list.push(`<br><a href="/watch?v=${vidnam}"><img id="${vids[i].ID.split('_')[0]}" src="/thumbnail/${vidnam}.jpg" width="20%" height="20%"></a>&nbsp;
        ${username}: <a href="/watch?v=${vidnam}">${vids[i].data}</a>
        <br>`);
    }

    res.send(`
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>johnjohnspub.com | criminally unsafe video streaming (how is this still up)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        body {
            width: 100%;
            height: 100%;
            margin: 40px auto;
            max-height: 1500px;
            max-width: 1500px;
            background-color: #000;
        }
        blockquote {
            margin: 40px auto;
            max-width: 500px;
            color: #fff;
            text-align: center;
        }
    </style>
    <meta content="johnjohnspub.com" property="og:title" />
    <meta content="world's most unsafe video streaming website" property="og:description" />
    <meta content="https://johnjohnspub.com" property="og:url" />
    <meta content="https://c.tenor.com/4ZkIt5fjuwIAAAAC/detroit.gif" property="og:image" />
    <meta content="#000000" data-react-helmet="true" name="theme-color" />
</head>

<body>
<center>
    <br id="top">
    <img style="width:100%;height:100%;max-width:600px;max-height:180px;" src="https://johnjohnspub.com/files/logo.gif">
    <br><br>

<hr style="color:white;margin: 40px auto;max-width: 500px;">
    
<a href="/video/">upload video</a>&nbsp;&nbsp;<a href="/video/img">upload image</a>&nbsp;&nbsp;<a href="/video/dioau">upload audio</a>&nbsp;&nbsp;<a href="#bottom">to bottom</a>
<blockquote style="text-align:left;">
<hr>
<br>
${list.join(" ")}
<br><br><hr>
</blockquote>
<a id="bottom" href="#top">to top</a>
<br><br><br>
</center>
    </body></html>
    `)
});

app.get('/video/img/', function (req, res) {
    res.sendFile(__dirname + "/img.html");
});

app.get('/video/updates/', function (req, res) {
    res.sendFile(__dirname + "/updates.html");
});

app.get("/video/embed/", async function (req, res) {
    // get video name
    var name = req.originalUrl.slice(13);

    res.send(`<iframe width="1004" height="753" src="https://johnjohnspub.com/files/${name}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`);
});

app.get("/watch", async function (req, res) {

    var fixd = req.originalUrl.slice(9);

    // funny comments section
    let comsection = "";

    if (db.fetch(`${fixd}_comment_0_username`) != undefined) {
        comsection += "<h3 style=\"margin-top:5%;margin-bottom:1%;\">Comments:</h3>";

        let index = 0;

        while (db.fetch(`${fixd}_comment_${index}_username`) != undefined) {
            let username = db.fetch(`${fixd}_comment_${index}_username`);
            let comment = db.fetch(`${fixd}_comment_${index}_content`);
            comsection += `<p style="font-size:150%;"><b>${username}:</b>&nbsp;${comment}</p>`;
            index++;
        }
    }

    db.add(`${fixd}_views`, 1);

    let quotes = [
        "new crap video player just dropped",
        "mate can I borrow a fiver",
        "I don't know what I'm doing",
        "I make this crap for free",
        `${req.ip.replace("::ffff:", "")}`
    ];

    let result = Math.floor(Math.random() * quotes.length);

    var viewCount = db.fetch(`${fixd}_views`);

    if (viewCount === null) viewCount = 0;

    let vids = db.all().filter((data) => data.ID.endsWith("video")).sort((a, b) => b.data - a.data);
    let title;
    let author;

    for (var i = vids.length - 1; i >= 0; i--) {
        let username = db.fetch(`${vids[i].ID.split('_')[0]}_username`);
        let filename = db.fetch(`${vids[i].ID.split('_')[0]}_fname`);
        let videotitle = db.fetch(`${vids[i].ID.split('_')[0]}_video`);
        if (filename === fixd) {
            title = videotitle;
            author = username;
            break;
        }
    }

    res.send(`
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />
        <meta name="twitter:card" content="player" />
        <meta name="twitter:site" content="tube.cristpz.eu" />
        <meta name="twitter:title" content="${author}" />
        <meta name="twitter:description" content="${title}" />
        <meta name="twitter:image" content="https://johnjohnspub.com/thumbnail/${fixd}.jpg" />
        <meta name="twitter:player" content="https://johnjohnspub.com/files/${fixd}" />
        <meta name="twitter:player:width" content="320" />
        <meta name="twitter:player:height" content="180" />
        <meta name="twitter:player:stream" content="https://johnjohnspub.com/files/${fixd}" />
        <meta name="twitter:player:stream:content_type" content="video/mp4" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>johnjohnspub.com | trim.13da2ae9-9bb2-47c8-90c4-6623b32e90f7.mov</title>
        <meta property="og:site_name" content="tube.cristpz.eu">
        <meta property="og:title" content="${author}">
        <meta property="og:image" content="https://johnjohnspub.com/thumbnail/${fixd}.jpg"> 
        <meta property="og:video:type" content="text/html">
        <meta property="og:video:url" content="https://johnjohnspub.com/files/${fixd}">
        <meta property="og:video:height" content="720">
        <meta property="og:video:width" content="1280">
        <meta property="og:type" content="video.movie">
        <meta name="twitter:card" content="summary_large_image">
        <meta property="og:title" content="${title}">
        <meta property="og:description" content="${fixd}">
        <meta property="og:url" content="https://johnjohnspub.com/watch?v=${fixd}">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <style>
            body {
                width: 100%;
                height: 100%;
                max-height: 1500px;
                margin: 40px auto;
                max-width: 1500px;
                background-color: #000;
                color: #fff;
            }
            
            .contents {
                position: relative;
                width: 90%;
                max-width: 1000px;
                justify-content: center;
                margin-inline: auto;
                background-color: black;
            }
        </style>
        <script type="text/javascript">
            function copy() {
    
                var copyText = document.getElementById("linkCopy");
    
                copyText.select();
                copyText.setSelectionRange(0, 99999);
    
                document.execCommand("copy");
    
            }
        </script>
        <link rel="stylesheet" href="https://johnjohnspub.com/files/style.css">
        <script src="https://johnjohnspub.com/files/script.js" defer></script>
    </head>
    
    <body>
        <div class="video-container paused" data-volume-level="high">
            <div class="video-controls-container">
                <div class="timeline-container">
                    <div class="timeline">
                        <div class="thumb-indicator"></div>
                    </div>
                </div>
                <div class="controls">
                    <button class="play-pause-btn">
              <svg class="play-icon" viewBox="0 0 24 24">
                <path fill="currentColor" d="M8,5.14V19.14L19,12.14L8,5.14Z" />
              </svg>
              <svg class="pause-icon" viewBox="0 0 24 24">
                <path fill="currentColor" d="M14,19H18V5H14M6,19H10V5H6V19Z" />
              </svg>
            </button>
                    <div class="volume-container">
                        <button class="mute-btn">
                <svg class="volume-high-icon" viewBox="0 0 24 24">
                  <path fill="currentColor" d="M14,3.23V5.29C16.89,6.15 19,8.83 19,12C19,15.17 16.89,17.84 14,18.7V20.77C18,19.86 21,16.28 21,12C21,7.72 18,4.14 14,3.23M16.5,12C16.5,10.23 15.5,8.71 14,7.97V16C15.5,15.29 16.5,13.76 16.5,12M3,9V15H7L12,20V4L7,9H3Z" />
                </svg>
                <svg class="volume-low-icon" viewBox="0 0 24 24">
                  <path fill="currentColor" d="M5,9V15H9L14,20V4L9,9M18.5,12C18.5,10.23 17.5,8.71 16,7.97V16C17.5,15.29 18.5,13.76 18.5,12Z" />
                </svg>
                <svg class="volume-muted-icon" viewBox="0 0 24 24">
                  <path fill="currentColor" d="M12,4L9.91,6.09L12,8.18M4.27,3L3,4.27L7.73,9H3V15H7L12,20V13.27L16.25,17.53C15.58,18.04 14.83,18.46 14,18.7V20.77C15.38,20.45 16.63,19.82 17.68,18.96L19.73,21L21,19.73L12,10.73M19,12C19,12.94 18.8,13.82 18.46,14.64L19.97,16.15C20.62,14.91 21,13.5 21,12C21,7.72 18,4.14 14,3.23V5.29C16.89,6.15 19,8.83 19,12M16.5,12C16.5,10.23 15.5,8.71 14,7.97V10.18L16.45,12.63C16.5,12.43 16.5,12.21 16.5,12Z" />
                </svg>
              </button>
                        <input class="volume-slider" type="range" min="0" max="1" step="any" value="1">
                    </div>
                    <div class="duration-container">
                        <div class="current-time">0:00</div>
                        /
                        <div class="total-time"></div>
                    </div>
                    <button class="speed-btn wide-btn">
              1x
            </button>
                    <button class="mini-player-btn">
              <svg viewBox="0 0 24 24">
                <path fill="currentColor" d="M21 3H3c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h18c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H3V5h18v14zm-10-7h9v6h-9z"/>
              </svg>
            </button>
                    <button class="theater-btn">
              <svg class="tall" viewBox="0 0 24 24">
                <path fill="currentColor" d="M19 6H5c-1.1 0-2 .9-2 2v8c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm0 10H5V8h14v8z"/>
              </svg>
              <svg class="wide" viewBox="0 0 24 24">
                <path fill="currentColor" d="M19 7H5c-1.1 0-2 .9-2 2v6c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2zm0 8H5V9h14v6z"/>
              </svg>
            </button>
                    <button class="full-screen-btn">
              <svg class="open" viewBox="0 0 24 24">
                <path fill="currentColor" d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/>
              </svg>
              <svg class="close" viewBox="0 0 24 24">
                <path fill="currentColor" d="M5 16h3v3h2v-5H5v2zm3-8H5v2h5V5H8v3zm6 11h2v-3h3v-2h-5v5zm2-11V5h-2v5h5V8h-3z"/>
              </svg>
            </button>
                </div>
            </div>
            <video src="https://johnjohnspub.com/files/${fixd}" type="video/mp4" autoplay></video>
        </div>
        <div class="contents">
            <h1 style="color:#fff;padding-top:1.75%;">${quotes[result]}</h1>
            <h2 style="color:#343a40">${viewCount} views</h2>
            <a style="font-size:28px;" href="/video/main">back to main</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="https://johnjohnspub.com/files/${fixd}" id="linkCopy"> <button onclick="copy()">To Clipboard</button>
            <p></p>
            <form style="margin-top:5%;" id="commentForm" action="/comment" method="post">
                <input type="hidden" name="videoname" value="${fixd}" required>
                <input type="text" name="username" placeholder="username" required>&nbsp;
                <input type="text" name="comment" placeholder="comment" required>
                <br><br>
                <input type="submit" value="Comment" name="submit"><br><br>
                <span id="status"></span>
            </form>
            ${comsection}
            <br><br>
        </div>
    </body>
    
    </html>
    `);
});

app.use("/video/images", async function (req, res) {

    let pics = db.all().filter((data) => data.ID.endsWith("photo")).sort((a, b) => b.data - a.data);
    let list = [];

    for (var i = pics.length - 1; i >= 0; i--) {
        let username = db.fetch(`${pics[i].ID.split('_')[0]}_username`);
        let img = db.fetch(`${pics[i].ID.split('_')[0]}_pname`);
        if (username === null) username = "anonymous";
        list.push(`<div id="${i + 1}" class="column" style="float:left;width:100%;"><p><a href="https://johnjohnspub.com/files/${img}"><img src="https://johnjohnspub.com/files/${img}" width="100" height="100" border="2px"></a>&nbsp;&nbsp;<p style="display:grid;color:#fff"><b>${username}</b> ${pics[i].data}</p></p></p></div>`);
    }

    res.send(`
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>johnjohnspub.com | criminally unsafe video streaming (how is this still up)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        body {
            width: 100%;
            height: 100%;
            margin: 40px auto;
            max-height: 1500px;
            max-width: 1500px;
            background-color: #000;
        }
        blockquote {
            margin: 40px auto;
            max-width: 500px;
            color: #fff;
            text-align: center;
        }
    </style>
    <meta content="johnjohnspub.com" property="og:title" />
    <meta content="world's most unsafe video streaming website" property="og:description" />
    <meta content="https://johnjohnspub.com/" property="og:url" />
    <meta content="https://c.tenor.com/4ZkIt5fjuwIAAAAC/detroit.gif" property="og:image" />
    <meta content="#000000" data-react-helmet="true" name="theme-color" />
</head>

<body>
<center>
    <br>
    <img style="width:100%;height:100%;max-width:600px;max-height:180px;" src="https://johnjohnspub.com/files/logo.gif">
    <br><br>

<hr style="color:white;margin: 40px auto;max-width: 500px;">
    
<a href="/video/">upload video</a>&nbsp;&nbsp;<a href="/video/img">upload image</a>&nbsp;&nbsp;<a href="/video/dioau">upload audio</a>
<hr style="color:white;margin: 40px auto;max-width: 500px;">
<br><br><br>
<div class="row" style="content:;clear: both;display: table;">
${list.join("")}
</div>
<br><br><hr style="color:white;margin: 40px auto;max-width: 500px;">
    </body></center></html>
    `)
});

app.use("/video/audio", async function (req, res) {

    let audio = db.all().filter((data) => data.ID.endsWith("audio")).sort((a, b) => b.data - a.data);
    let list = [];

    for (var i = audio.length - 1; i >= 0; i--) {
        let username = db.fetch(`${audio[i].ID.split('_')[0]}_username`);
        let adio = db.fetch(`${audio[i].ID.split('_')[0]}_aname`);
        if (adio.includes("ogg")) type = "ogg"
        else type = "mpeg";
        if (username === null) username = "anonymous";
        list.push(`<div id="${i + 1}" class="column" style="float:center;width:100%;"><p><a href="https://johnjohnspub.com/files/${adio}"><audio controls><source src="https://johnjohnspub.com/files/${adio}" type="audio/${type}"></audio></a>&nbsp;&nbsp;<p style="display:grid;text-align:center;color:#fff"><b>${username}</b> ${audio[i].data}</p></p></p></div>`);
    }

    res.send(`
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>johnjohnspub.com | criminally unsafe video streaming (how is this still up)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        body {
            width: 100%;
            height: 100%;
            margin: 40px auto;
            max-height: 1500px;
            max-width: 1500px;
            background-color: #000;
        }
        blockquote {
            margin: 40px auto;
            max-width: 500px;
            color: #fff;
            text-align: center;
        }
    </style>
    <meta content="johnjohnspub.com" property="og:title" />
    <meta content="world's most unsafe video streaming website" property="og:description" />
    <meta content="https://johnjohnspub.com/" property="og:url" />
    <meta content="https://c.tenor.com/4ZkIt5fjuwIAAAAC/detroit.gif" property="og:image" />
    <meta content="#000000" data-react-helmet="true" name="theme-color" />
</head>

<body>
<center>
    <br>
    <img style="width:100%;height:100%;max-width:600px;max-height:180px;" src="https://johnjohnspub.com/files/logo.gif">
    <br><br>

<hr style="color:white;margin: 40px auto;max-width: 500px;">
    
<a href="/video/">upload video</a>&nbsp;&nbsp;<a href="/video/img">upload image</a>&nbsp;&nbsp;<a href="/video/dioau">upload audio</a>
<hr style="color:white;margin: 40px auto;max-width: 500px;">
<br><br><br>
<div class="row" style="content:;clear: both;display: table;">
${list.join("")}
</div>
<br><br><hr style="color:white;margin: 40px auto;max-width: 500px;">
    </body></center></html>
    `)
});

app.get("/video/dox", async function (req, res) {

    let { body } = await superagent
        .get(`http://api.ipstack.com/${req.ip.replace("::ffff:", "")}?access_key=b7be4f0107783dda48188ee3bd462be6`);

    res.send(`
        <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta property="og:image" content="https://johnjohnspub.com/files/dox.jpg" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="800" />
    <meta property="og:title" content="you should dox yourself now" />
    <meta property="og:url" content="https://johnjohnspub.com/dox" />
    <title>johnjohnspub.com | doxxed</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        body {
            width: 100%;
            height: 100%;
            max-height: 1500px;
            margin: 40px auto;
            max-width: 1500px;
            background-color: #000;
        }
    </style>
<script>
function copy() {

  var copyText = document.getElementById("linkCopy");

  copyText.select();
  copyText.setSelectionRange(0, 99999); 

  document.execCommand("copy");
  
}
</script>
</head>

<body>
   <center> 
   <br><br><br>
   <p>IP: ${body.ip}<br><br>Type: ${body.type}<br><br>Continent: ${body.continent_name}<br><br>Country: ${body.country_name}<br><br>Region: ${body.region_name}<br><br>City: ${body.city}<br><br>Zip Code: ${body.zip}<br><br>Latitude: ${body.latitude}<br><br>Longitude: ${body.longitude}</p>
   <br><br><br>
   </center>
</body>
</html>
`)

});


/*
VIDEO HOSTING END
*/

app.listen(80);
server.listen(443);

client.on('messageCreate', async message => {
    if (message.author.bot) return;
    if (!message.content.startsWith("p!")) return;
    const args = message.content.slice(settings.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    let guildQueue = client.player.getQueue(message.guild.id);

    if (command === 'getui' || command === 'gu' || command === 'uikey') {
        let random_key = Math.floor(100000000 + Math.random() * 900000000);
        await db.set(`${random_key}_${message.guild.id}`, message.author.id);
        message.author.send(`your link for **${message.guild.name}** is: https://johnjohnspub.com/pbmui.html?guild_id=${message.guild.id}&key=${random_key}`);
    }

    if (command === 'sticker') {
        if (message.stickers.size > 0) message.channel.send(message.stickers.map(s => s.url).join("\n"));
    }

    if (command === 'queue' || command === 'q') {
        if (!guildQueue) return message.channel.send('nothing is playing right now');
        let list = guildQueue.songs.map((song, i) => {
            if (song) return `${i === 0 ? 'now playing' : `${i + 1}`} - ${song.name} | ${song.author}`
        }).join('\n');
        for (let i = 0; i < list.length; i += 2000) {
            const toSend = list.substring(i, Math.min(list.length, i + 2000));
            message.channel.send(toSend);
        }
    }

    if (command === 'nowplaying' || command === 'np') {
        if (!guildQueue) return message.channel.send('nothing is playing right now');
        const ProgressBar = guildQueue.createProgressBar();
        message.channel.send(`Now playing: \`${guildQueue.nowPlaying}\` || Volume: \`${guildQueue.volume}%\`\n${ProgressBar.prettier}`);
    }

    if (message.member.voice.channel) {
        if (command === 'play' || command === 'p') {
            if ((args[0].includes("list=") && !args[0].includes("start_radio=1")) || args[0].includes("/album/") || args[0].includes("/playlist/")) {
                let queue = client.player.createQueue(message.guild.id, {
                    data: {
                        channel: message.channel, guild_id: message.guild.id
                    }
                });
                await queue.join(message.member.voice.channel);
                let song = await queue.playlist(args.join(' ')).catch(err => {
                    console.log(err);
                    if (!guildQueue)
                        queue.stop();
                });
            } else if (args[1] === "--file") {
                const player = createAudioPlayer();

                joinVoiceChannel({
                    channelId: message.member.voice.channel.id,
                    guildId: message.guild.id,
                    adapterCreator: message.guild.voiceAdapterCreator
                }).subscribe(player);

                message.guild.me.voice.setRequestToSpeak(true);
                const resource = createAudioResource(args[0]);
                player.play(resource);
            } else {
                let queue = client.player.createQueue(message.guild.id, {
                    data: {
                        channel: message.channel, guild_id: message.guild.id
                    }
                });
                await queue.join(message.member.voice.channel);
                let song = await queue.play(args.join(' ')).catch(err => {
                    console.log(err);
                    if (!guildQueue)
                        queue.stop();
                });
            }
        }

        if (command === 'search' || command === 'ps') {
            const r = await yts(args.join(' '));

            const videos = r.videos.slice(0, 3)
            let pick = [];
            videos.forEach(function (v) {
                const views = String(v.views).padStart(10, ' ')
                message.channel.send(`${views} | ${v.title} (${v.timestamp}) | ${v.author.name}`)
                pick.push(v.url);
            });

            const filter = (m) => m.author.id === message.author.id;

            const collector = message.channel.createMessageCollector({
                filter,
                max: 5,
                time: 1000 * 20,
            });

            message.channel.send("pick a video, send a number (1-3)\n" + pick.join('\n'))

            collector.on('collect', async (collected) => {
                let d = collected.content;
                let res;
                if (d == '1') {
                    res = pick[0];
                } else if (d == '2') {
                    res = pick[1];
                } else if (d == '3') {
                    res = pick[2];
                }
                let queue = client.player.createQueue(message.guild.id, {
                    data: {
                        channel: message.channel, guild_id: message.guild.id
                    }
                });
                await queue.join(message.member.voice.channel);
                let song = await queue.play(res).catch(err => {
                    console.log(err);
                    if (!guildQueue)
                        queue.stop();
                });
            });
        }

        if (command === 'skip' || command === 's') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.skip();
            message.channel.send('skipped');
        }

        if (command === 'stop' || command === 'fuckoff' || command === 'die') {
            guildQueue.stop();
        }

        if (command === 'loop') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            if (args[0] === '0') {
                guildQueue.setRepeatMode(RepeatMode.DISABLED);
                message.channel.send("loop: done");
            } else if (args[0] === '1') {
                guildQueue.setRepeatMode(RepeatMode.SONG);
                message.channel.send("loop: single");
            } else if (args[0] === '2') {
                guildQueue.setRepeatMode(RepeatMode.QUEUE);
                message.channel.send("loop: queue");
            }
        }

        if (command === 'volume' || command === 'v') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.setVolume(parseInt(args[0]));
            message.channel.send("volume set to `" + args[0] + "%`");
        }

        if (command === 'seek' || command === 'goto') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.seek(parseInt(args[0]) * 1000);
            message.channel.send(`moving to: ${args[0]} sec`)
        }

        if (command === 'clearqueue' || command === 'clear') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.clearQueue();
            message.channel.send(`queue rekt`)
        }

        if (command === 'shuffle') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.shuffle();
            message.channel.send(`shuffling queue`)
        }

        if (command === 'pause') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.setPaused(true);
            message.channel.send("paused");
        }

        if (command === 'resume') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.setPaused(false);
            message.channel.send("resumed");
        }

        if (command === 'remove') {
            guildQueue.remove(parseInt(args[0]) - 1);
            message.channel.send("removed song #" + args[0]);
        }
    }
});

client.on('interactionCreate', async interaction => {
    if (!interaction.isCommand()) return;

    let guildQueue = client.player.getQueue(interaction.guildId);
    const query = interaction.options.getString("input");
    const integer = interaction.options.getInteger('int');

    let user = await interaction.member.fetch();
    let channel = await user.voice.channel;

    if (interaction.commandName === 'khaled' || interaction.commandName === 'k') {
        await interaction.reply("https://tenor.com/view/dj-khaled-meme-surprise-shock-gif-21311476");
    }

    if (user.voice.channel) {
        if (interaction.commandName === 'play') {
            let queue = client.player.createQueue(interaction.guildId);
            await queue.join(message.member.voice.channel);
            let song = await queue.play(query).catch(err => {
                console.log(err);
            });
        }
        if (interaction.commandName === 'playlist') {
            let queue = client.player.createQueue(interaction.guildId);
            await queue.join(message.member.voice.channel);
            let song = await queue.playlist(query).catch(err => {
                console.log(err);
            });
        }
        if (interaction.commandName === 'skip') {
            if (!guildQueue) return interaction.reply('nothing is playing right now');
            guildQueue.skip();
            await interaction.reply('skipped');
        }
        if (interaction.commandName === 'die') {
            guildQueue.stop();
            await interaction.reply('ok');
        }
        if (interaction.commandName === 'shuffle') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.shuffle();
            await interaction.reply('shuffling queue');
        }
        if (interaction.commandName === 'volume') {
            guildQueue.setVolume(parseInt(num));
            await interaction.reply("volume set to `" + num + "%`");
        }
        if (interaction.commandName === 'loop') {
            if (num === '0') {
                guildQueue.setRepeatMode(RepeatMode.DISABLED);
                await interaction.reply("loop: done");
            } else if (num === '1') {
                guildQueue.setRepeatMode(RepeatMode.SONG);
                await interaction.reply("loop: single");
            } else if (num === '2') {
                guildQueue.setRepeatMode(RepeatMode.QUEUE);
                await interaction.reply("loop: queue");
            }
        }
        if (interaction.commandName === 'seek') {
            guildQueue.seek(parseInt(num) * 1000);
            await interaction.reply(`moving to: ${num} sec`)
        }
        if (interaction.commandName === 'clear') {
            if (!guildQueue) return message.channel.send('nothing is playing right now');
            guildQueue.clearQueue();
            await interaction.reply(`queue rekt`)
        }
        if (interaction.commandName === 'np') {
            if (!guildQueue) return interaction.reply('nothing is playing right now');
            const ProgressBar = guildQueue.createProgressBar();
            await interaction.reply(`Now playing: \`${guildQueue.nowPlaying}\` || Volume: \`${guildQueue.volume}%\`\n${ProgressBar.prettier}`);
        }
        if (interaction.commandName === 'q') {
            if (!guildQueue) return interaction.reply('nothing is playing right now');
            let list = guildQueue.songs.map((song, i) => {
                if (song) return `${i === 0 ? 'now playing' : `${i + 1}`} - ${song.name} | ${song.author}`
            }).join('\n');
            for (let i = 0; i < list.length; i += 2000) {
                const toSend = list.substring(i, Math.min(list.length, i + 2000));
                await interaction.reply(toSend);
            }
        }
        if (interaction.commandName === 'pause') {
            if (!guildQueue) return interaction.reply('nothing is playing right now');
            guildQueue.setPaused(true);
            await interaction.reply("paused");
        }
        if (interaction.commandName === 'resume') {
            if (!guildQueue) return interaction.reply('nothing is playing right now');
            guildQueue.setPaused(false);
            await interaction.reply("resumed");
        }
        if (interaction.commandName === 'remove') {
            guildQueue.remove(parseInt(num));
            await interaction.reply("removed song #" + num);
        }
    }
});

client.login(settings.token);
